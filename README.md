
# aula-04

O objetivo e mostrar o funcionamento da biblioteca `react-router` para trabalhar com rotas.

ref: https://reactrouter.com/en/main

Requisitos:

- Devera criar o componente `Card` generico seguindo o mesmo designer-system da aula-01

- Devera criar um componente `Details` e ele deve renderizar as seguintes propiedades: images = array, title = string, price = number, description = string
estilizacao livre.

- Devera adicionar mais uma classe ao componente `Button` criado na aula-02 que se chamara `info`. com essa classe o button devera ser renderizado na cor azul.

- Devera abstrair as chamadas a api no arquivo `api.js`
- Devera ter duas funcoes `getProducts` que retorna um array de produtos e `getProduct` que recebera como prop o id do produto e retornara somente um produto.

- Devera renderizar todos produtos no componente `App` e cada produto devera ser renderizado no componente `Card`, com as props: title, description e image assim como foi feito na aplicacao legada. No lugar do `Button` adicionar ao carrinho, utilizar a classe `info` e o titulo deve ser `Ir para produto`

- Ao clicar em `Ir para produto` devera passar o id do produto e fazer uma chamada para a api passando o id do produto e o renderizar no componente `Details`



